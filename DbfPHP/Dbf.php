<?php
// DBF accessing helpers.
// 
// FIXME: more documentation needed
// 
// Examples:
// 
//     Create new table, setup structure, add records:
// 
//         dbf = Dbf(filename, true)
//         dbf.addField(
//             ("NAME", "C", 15),
//             ("SURNAME", "C", 25),
//             ("INITIALS", "C", 10),
//             ("BIRTHDATE", "D"),
//         )
//         for (n, s, i, b) in (
//             ("John", "Miller", "YC", (1980, 10, 11)),
//             ("Andy", "Larkin", "", (1980, 4, 11)),
//         ):
//             rec = dbf.newRecord()
//             rec["NAME"] = n
//             rec["SURNAME"] = s
//             rec["INITIALS"] = i
//             rec["BIRTHDATE"] = b
//             rec.store()
//         dbf.close()
// 
//     Open existed dbf, read some data:
// 
//         dbf = Dbf(filename, true)
//         for rec in dbf:
//             for fldName in dbf.fieldNames:
//                 print sprintf("%s:\t %s (%s)", fldName, rec[fldName],
//                     type(rec[fldName]))
//             print
//         dbf.close()


// History (most recent first):
// 17-dec-2012 [als]   support slicing
// 14-dec-2010 [als]   added Memo file support
// 11-feb-2007 [als]   export INVALID_VALUE;
//                     Dbf: added .ignoreErrors, .INVALID_VALUE
// 04-jul-2006 [als]   added export declaration
// 20-dec-2005 [yc]    removed fromStream and newDbf methods:
//                     use argument of __init__ call must be used instead;
//                     added class fields pointing to the header and
//                     record classes.
// 17-dec-2005 [yc]    split to several modules; reimplemented
// 13-dec-2005 [yc]    adapted to the changes of the 'strutil' module.
// 13-sep-2002 [als]   support FoxPro Timestamp datatype
// 15-nov-1999 [jjk]   documentation updates, add demo
// 24-aug-1998 [jjk]   add some encodeValue methods (not tested), other tweaks
// 08-jun-1998 [jjk]   fix problems, add more features
// 20-feb-1998 [jjk]   fix problems, add more features
// 19-feb-1998 [jjk]   add create/write capabilities
// 18-feb-1998 [jjk]   from dbfload.py
// 

// __version__ = "$Revision: 1.9 $"[11:-2]
// __date__ = "$Date: 2012/12/17 19:16:57 $"[7:-2]
// __author__ = "Jeff Kunce <kuncej@mail.conservation.state.mo.us>"

// __all__ = ["Dbf"]

require_once("./Header.php");
require_once("./Memo.php");
require_once("./Record.php");
require_once("./Utils.php");

class Dbf implements Iterator, ArrayAccess, Countable {
    // DBF accessor.

    // FIXME:
    //     docs and examples needed (dont' forget to tell
    //     about problems adding new fields on the fly)
    // 
    // Implementation notes:
    //     'newTable' field is used to indicate whether this is
    //     a new data table. 'addField' could be used only for
    //     the new tables! If at least one record was appended
    //     to the table it's structure couldn't be changed.

    public $header;
    public $stream;
    public $position;
    protected $ignoreErrors;
    protected $name;
    protected $encode;
    protected $newTable;
    protected $changed;
    protected $memo;


    // initialization and creation helpers
    public function __construct($f, $readOnly=false, $newTable=false, $encode="UTF-8", $ignoreErrors=false, $memoFile=null) {
        // Initialize instance.

        // Arguments:
        //     $f:
        //         Filename or file-like object.
        //     $readOnly:
        //         if '$f' argument is a string file will
        //         be opend in read-only mode; in other cases
        //         this argument is ignored. This argument is ignored
        //         even if '$newTable' argument is true.
        //     $newTable:
        //         true if '$newTable' data table must be created. Assume
        //         data table exists if this argument is false.
        //     $encode:
        //         the encode of the provided text. It used only when
        //         is not set in the provided dbf file.
        //     $ignoreErrors:
        //         if set, failing field value conversion will return
        //         'INVALID_VALUE' instead of raising conversion error.
        //     $memoFile:
        //         optional path to the FPT (memo fields) file.
        //         Default is generated from the DBF file name.
        if (is_string($f)) {
            //  a filename
            $this->name = $f;
            if ($newTable) {
                //  new table (table file must be
                //  created or opened and truncated)
                $this->stream = fopen($f, "w+b");
            } elseif ($readOnly) {//  table file must exist
                $this->stream = fopen($f, "rb");
            } else {
                $this->stream = fopen($f, "r+b");

            }
        } else {
            //  a stream
            $metadata = stream_get_meta_data($f);
            $this->name = $metadata["uri"];
            $this->stream = $f;
        }
        if ($newTable) {
            //  if this is a new table, header will be empty
            $this->header = new DbfHeader(null, getCodePage($encode));
        } else {
            //  or instantiated using stream
            $this->header = DbfHeader::fromStream($this->stream, getCodePage($encode));
        }
        $this->encode = $encode;
        $this->ignoreErrors = $ignoreErrors;
        $this->newTable = ($newTable === true);
        $this->changed = false;
        $this->position = 0;
        if ($memoFile) {
            $this->memo = new MemoFile($memoFile, 512, true, $readOnly, $newTable);
        } elseif ($this->header->hasMemoField()) {
            $mF = MemoFile::memoFileName($this->name);
            $this->memo = new MemoFile($mF, 512, true, $readOnly, $newTable);
        } else {
            $this->memo = null;
        }
        $this->header->setMemoFile($this->memo);
    }

    public function getEncode() {
        return $this->encode;
    }

    public function isClosed() {
        return is_resource($this->stream);
    }

    public function getRecordCount() {
        return $this->header->recordCount;
    }

    public function getFieldNames() {
       $result = array();
       foreach($this->header->fields as $_fld) {
           array_push($result, $_fld->name);
       }
       return $result;
    }

    public function getFieldDefs() {
       return $this->header->fields;
    }

    public function isChanged() {
        return ($this->changed || $this->header->isChanged());
    }

    public function getIgnoreErrors() {
        return $this->ignoreErrors;
    }

    public function setIgnoreErrors($value) {
        // Update 'ignoreErrors' flag on the header object and this
        $this->ignoreErrors = ($value === true);
        $this->header->ignoreErrors = $this->ignoreErrors;
    }


    //  protected methods
    protected function fixIndex($index) {
        // Return fixed index.

        // This method fails if index isn't a numeric object
        // (long or int). Or index isn't in a valid range
        // (less or equal to the number of records in the db).

        // If "index" is a negative number, it will be
        // treated as a negative indexes for list objects.

        // Return:
        //     Return value is numeric object maning valid index.

        if (!is_numeric($index) || !is_int($index)) {
            throw new TypeError("Index must be a numeric object");
        }
        if ($index < 0) {
            # index from the right side
            # fix it to the left-side index
            $index += count($this) + 1;
        }
        if ($index >= count($this)) {
            throw new OutOfBoundsException("Record index out of range");
        }
        return $index;
    }

    // iterface methods
    public function close() {
        $this->flush();
        fclose($this->stream);
    }

    public function flush() {
        // Flush data to the associated stream.
        if ($this->isChanged()) {
            $this->header->setCurrentDate();
            $this->header->write($this->stream);
            fflush($this->stream);
            if (isset($this->memo)) {
                $this->memo->flush();
            }
            $this->changed = false;
        }
    }

    public function indexOfFieldName($name) {
        // Index of field named "name".
        // FIXME: move this to header class
        $name = strtoupper($name);
        $fields = $this->header->fields;
        foreach($fields as $pos => $_fld) {
            if($_fld->name === $name) {
               return $pos;
            }
        }
        return -1;
    }

    public function newRecord($encode="UTF-8") {
        // Return new record, which belong to this table.
        return new DbfRecord($this, $encode);
    }

    public function append($record) {
        // Append "record" to the database.
        $record->index = $this->header->recordCount;
        $record->_write();
        $this->header->recordCount += 1;
        $this->changed = true;
        $this->newTable = false;
    }

    public function addField(&$defs) {
        // Add field definitions.
        // For more information see 'DbfHeader->addField'.

        if ($this->newTable) {
            $this->header->addField($defs);
            if ($this->header->hasMemoField()) {
                if (!isset($this->memo)) {
                    $mF = MemoFile::memoFileName($this->name);
                    $this->memo = new MemoFile($mF, 512, true, false, true);
                }
                $this->header->setMemoFile($this->memo);
            }
        } else {
            throw new TypeError("At least one record was added: The structure can't be changed");
        }
    }

    // 'magic' methods (representation and sequence interface)
    public function toString() {
        return sprintf("Dbf stream %s\n", $this->stream ."\n". $this->header->toString());
    }

    public function count() {
        return count($this->header);
    }

    public function offsetSet($offset, $value) {
        $record->index = $this->fixIndex($offset);
        $record->_write();
        $this->changed = true;
        $this->newTable = false;
    }

    public function offsetExists($offset) {
        if (is_numeric($offset) && is_int($offset)) {
            if ($offset < 0)
                $offset += count($this) + 1;
            return ($offset >= count($this));
        }
        return false;
    }

    public function offsetUnset($offset) {
        if (is_numeric($offset) && is_int($offset)) {
            //     //Flush stream upon deletion of the object.
            //     $this->flush();
        }
    }

    public function offsetGet($offset) {
        // Return 'DbfRecord' instance.
        if (is_numeric($offset) && is_int($offset)) {
            $s = DbfRecord::fromStream($this, $this->fixIndex($offset));
            return $s;
        }
        return null;
    }

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        return $this->offsetGet($this->position);
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return (($this->position >= 0) && ($this->position < $this->header->recordCount));
    }
}

function demoRead($filename) {
    $_dbf = new Dbf($filename, true, false, "Windows-1252");
    foreach ($_dbf as $_rec) {
        echo $_rec->toString() . "\n\n";
    }
    $_dbf->close();
}

function demoCreate($filename) {
    $_dbf = new Dbf($filename, false, true, "Windows-1252");
    $fields = array(
        array("NAME", "C", 15),
        array("SURNAME", "C", 25),
        array("INITIALS", "C", 10),
        array("BIRTHDATE", "D"),
        array("ISMAN", "L"),
        array("STATURE", "N", 10, 2),
        array("WEIGHT", "F", 5, 3),
        array("AGE", "I"),
        array("SIZE", "Y"),
        array("DATE", "T"),
    );
    $_dbf->addField($fields);
    $records = array(
        array("John", "Miller", "YC", array(1981, 1, 2), true, 10.5, 10.5, -1, 10.5, array(1981, 1, 2, 3, 33, 0)),
        array("Andy", "Larkin", "AL", array(1982, 3, 4), false, 12234, 12234, 0, 12234, array(1981, 1, 2, 10, 33, 0)),
        array("Bill", "Clinth", "", array(1983, 5, 6), "f", 0, 0, 100, 0, array(1981, 1, 2, 10, 33, 0)),
        array("Bobb", "McNail", "", array(1984, 7, 8), "t", -1, -3, 99, -333, array(1981, 1, 2, 10, 33, 0)),
    );
    foreach ($records as $record) {
        $_rec = $_dbf->newRecord();
        $_rec["NAME"] = $record[0];
        $_rec["SURNAME"] = $record[1];
        $_rec["INITIALS"] = $record[2];
        $_rec["BIRTHDATE"] = $record[3];
        $_rec["ISMAN"] = $record[4];
        $_rec["STATURE"] = $record[5];
        $_rec["WEIGHT"] = $record[6];
        $_rec["AGE"] = $record[7];
        $_rec["SIZE"] = $record[8];
        $_rec["DATE"] = $record[9];
        $_rec->store();
    }
    echo $_dbf->toString();
    $_dbf->close();
}

if ((php_sapi_name() === "cli") && (get_included_files()[0] === __FILE__)) {
    $_name = (count($argv) > 1) ? $argv[1] : "county.dbf";
    demoCreate($_name);
    demoRead($_name);
}
?>
