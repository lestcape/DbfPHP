<?php
// Memo file support.

// History (most recent first):
// 13-dec-2010 [als]   created

// __version__ = "$Revision: 1.3 $"[11:-2]
// __date__ = "$Date: 2010/12/15 08:08:23 $"[7:-2]

//  Note: the data class is exported for TYPE constants.
// __all__ = ["MemoFile", "MemoData"]

/*import os
import struct*/
require_once("./Utils.php");

//FIXME: Extends string? class MemoData(str):
class MemoData {
    // Data read from or written to Memo file.
    // 
    // This is 8-bit string data with additional attribute
    // type which can accept values of the TYPE_* constants.


    protected $TYPE_PICTURE = 0;
    protected $TYPE_MEMO = 1;
    protected $TYPE_OBJECT = 2;
    protected $TYPE_NULL = 160;

    //protected $type = $TYPE_MEMO;

    public function __construct($cls, $value, $type=1) {
    //def __new__(cls, value, type=TYPE_MEMO):
    //    _obj = super(MemoData, cls).__new__(cls, value);
    //    _obj.type = type;
    //    return _obj;
    }
}

class MemoFile {
    // Memo file object

    //  End Of Text
    protected $EOT = "\x1A\x1A";

    protected $is_fpt;
    protected $name;
    protected $stream;
    protected $blocksize;
    protected $tail;

    public function __construct($f, $blocksize=512, $fpt=true,
            $readOnly=false, $newTable=false) {
        // Initialize instance.
        // 
        // Arguments:
        //     $f:
        //         Filename or file-like object.
        //     $blocksize:
        //         Size of blocks in the Memo file.
        //         Used for new files only; ignored if file already exists.
        //     $fpt:
        //         true if file format is FoxPro Memo file
        //         (file blocks start with type and length fields).
        //     $readOnly:
        //         If true, open existing files read-only.
        //         Ignored if "$f" is a file object of if "$newTable" is true.
        //     $newTable:
        //         true to create new memo file,
        //         false to open existing file.

        $this->is_fpt = $fpt;
        if (is_string($f)) {
            //  a filename
            $this->name = $f;
            if ($newTable) {
                $this->stream = fopen($f, "w+b");
            } elseif ($readOnly) {
                $this->stream = fopen($f, "rb");
            } else {
                $this->stream = fopen($f, "r+b");
            }
        } else {
            //  a stream
            $metadata = stream_get_meta_data($f);
            $this->name = $metadata["uri"];
            $this->stream = $f;
        }
        fseek($this->stream, 0);
        if ($newTable) {
            if (!isset($this->is_fpt)) {
                $this->blocksize = 512;
            //  http://msdn.microsoft.com/en-US/library/d6e1ah7y%28v=VS.80%29.aspx
            } elseif ($blocksize == 0) {
                $this->blocksize = 1;
            } elseif ($blocksize <= 32) {
                $this->blocksize = 512 * $blocksize;
            } else {
                $this->blocksize = $blocksize;
            }
            $this->tail = 512 / $this->blocksize;

            $packed  = structPack(">LHH", array($this->tail, 0, $this->blocksize));
            $packed .= str_pad("", 8, chr(0)) . "\x03" . str_pad("", 495, chr(0));
            fwrite($this->stream, $packed);
        } else {
            $matrix = structUnpack(">LHH", fread($this->stream, 8));
            $this->tail = 0;
            $_zero = 0;
            $this->blocksize = 0;
            if (!isset($this->is_fpt)) {
                //  In DBT files, block size is fixed to 512 bytes
                $this->blocksize = 512;
            }
        }
    }

    // @staticmethod
    public static function memoFileName($name, $isFpt=true) {
        // Return Memo file name for given DBF file name
        // 
        // Arguments:
        //     $name:
        //         Name of DBF file.  FoxPro file extensions
        //         like SCX or DBC are supported.
        //     $isFpt:
        //         true if file is FoxPro Memo file.
        //         If isFpt is false, DBF memos have
        //         extension DBT instead of FPT.

        $fileInfo = pathinfo($name);
        $_ext = $fileInfo['extension'];
        $_filename = $partes_ruta['filename'];
        if (array_search(strtoupper($_ext), array("", ".DBF")) !== false) {
            if ($isFpt) {
                return $_filename + ".FPT";
            }
            return $_filename + ".DBT";
        }
        return substr($name, 0, -1) . "T";
    }

    public function read($blocknum) {
        // Read the block addressed by blocknum
        // 
        // Return a MemoData object.

        fseek($this->stream, $this->blocksize * $blocknum);
        if ($this->is_fpt) {
            $matrix = structUnpack(">LL", fread($this->stream, 8));
            $_type = 0;
            $_len = 0;
            if ($_type == MemoData.TYPE_NULL) {
                $_value = '';
            } else {
                $_value = fread($this->stream, $_len);
            }
        } else { //  DBT
            $_type = MemoData.TYPE_MEMO;
            fseek($this->stream, $this->blocksize * $blocknum);
            $_value = '';
            /*while ($this->EOT not in $_value) {
                $_value += fread($this->stream, $this->blocksize);
            }*/
            $_value = substr($_value, 0, $_value.find($this->EOT));
        }
        return new MemoData($_value, $_type);
    }

    public function write($value) {
        // Write a value to FPT file, return starting block number
        // 
        // The value argument may be simple string or a MemoData object.
        // In the former case value type is assumed to be TYPE_MEMO.

        $_rv = $this->tail;
        fseek($this->stream, $this->blocksize * $_rv);
        if ($this->is_fpt) {
            $_length = strlen($value) + 8;
            $_type = getattr($value, "type", MemoData.TYPE_MEMO);
            $packed  = structPack(">LL", array(_type, strlen($value)));
            $packed .= $value;
            fwrite($this->stream, $packed);
        } else {
            $_length = strlen($value) + 2;
            fwrite($this->stream, $value . $this->EOT);
        }
        // $_cnt = int(math.ceil(float($_length) / $this->blocksize));
        $_cnt = ($_length + $this->blocksize - 1) / $this->blocksize;
        fwrite($this->stream, str_pad("", $_cnt * $this->blocksize - $_length, chr(0)));
        $this->tail += $_cnt;
        fseek($this->stream, 0);
        $packed = structPack(">L", $this->tail);
        fwrite($this->stream, $packed);
        return $_rv;
    }

    public function flush() {
        // Flush data to the associated stream.
        fflush($this->stream);
    }
}
?>
