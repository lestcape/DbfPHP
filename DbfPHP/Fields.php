<?php
// DBF fields definitions.
// 
// TODO:
//   - make memos work

// History (most recent first):
// 14-dec-2010 [als]   support reading and writing Memo fields;
//                     .toEncodeString: write field offset
// 26-may-2009 [als]   DbfNumericFieldDef.decodeValue: strip zero bytes
// 05-feb-2009 [als]   DbfDateFieldDef.encodeValue: empty arg produces empty date
// 16-sep-2008 [als]   DbfNumericFieldDef decoding looks for decimal point
//                     in the value to select float or integer return type
// 13-mar-2008 [als]   check field name length in constructor
// 11-feb-2007 [als]   handle value conversion errors
// 10-feb-2007 [als]   DbfFieldDef: added .rawFromRecord()
// 01-dec-2006 [als]   Timestamp columns use null for empty values
// 31-oct-2006 [als]   support field types 'F' (float), 'I' (integer)
//                     and 'Y' (currency);
//                     automate export and registration of field classes
// 04-jul-2006 [als]   added export declaration
// 10-mar-2006 [als]   decode empty values for Date and Logical fields;
//                     show field name in errors
// 10-mar-2006 [als]   fix Numeric value decoding: according to spec,
//                     value always is string representation of the number;
//                     ensure that encoded Numeric value fits into the field
// 20-dec-2005 [yc]    use field names in upper case
// 15-dec-2005 [yc]    field definitions moved from `dbf`.

// __version__ = "$Revision: 1.15 $"[11:-2]
// __date__ = "$Date: 2010/12/14 11:04:49 $"[7:-2]

// __all__ = ["lookupFor",] # field classes added at the end of the module

//import datetime
//import struct
//import sys

//from memo import MemoData
//import utils
require_once("./Memo.php");
require_once("./Utils.php");

// abstract definitions

class DbfFieldDef /*implements Iterator*/ {
    // Abstract field definition.
    // 
    // Child classes must override "type" class attribute to provide datatype
    // infromation of the field definition. For more info about types visit
    // 'http://www.clicketyclick.dk/databases/xbase/format/data_types.html'
    // 
    // Also child classes must override "defaultValue" field to provide
    // default value for the field value.
    // 
    // If child class has fixed length "length" class attribute must be
    // overriden and set to the valid value. null value means, that field
    // isn't of fixed length.
    // 
    // Note: "name" field must not be changed after instantiation.

    //  field type. for more information about fields types visit
    //  'http://www.clicketyclick.dk/databases/xbase/format/data_types.html'
    //  must be overriden in child classes
    public $typeCode = null;

    //  default value for the field. this field must be
    //  overriden in child classes
    public $defaultValue = null;

    //  length of the field, null in case of variable-length field,
    //  or a number if this field is a fixed-length field
    public $length = null;
    public $name;
    public $ignoreErrors;
    public $decimalCount;
    public $end;
    public $start;

    public function __construct($name, $length=null, $decimalCount=null,
        $start=null, $stop=null, $ignoreErrors=false) {
        // Initialize instance.

        //assert $this->typeCode is not null, "Type code must be overriden";
        //assert $this->defaultValue is not null, "Default value must be overriden";

        // fix arguments
        if (strlen($name) > 10) {
            $error = sprintf("Field name \"%s\" is too long", $name);
            throw new InvalidArgumentException($error);
        }
        $name = strtoupper((string)$name);
        if ($this->length === null) {
            if (!isset($length)) {
                $error = sprintf("[%s] Length isn't specified", $name);
                throw new InvalidArgumentException($error);
            }
            $length = intval($length);
            if ($length <= 0) {
                $error = sprintf("[%s] Length must be a positive integer", $name);
                throw new InvalidArgumentException($error);
            }
        } else {
            $length = $this->length;
        }
        if (!isset($decimalCount)) {
            $decimalCount = 0;
        }
        // set fields
        $this->name = $name;
        //  FIXME: validate length according to the specification at
        //  http://www.clicketyclick.dk/databases/xbase/format/data_types.html
        $this->length = $length;
        $this->decimalCount = $decimalCount;
        $this->ignoreErrors = $ignoreErrors;
        $this->start = $start;
        $this->end = $stop;
    }

    //  true if field data is kept in the Memo file
    public function isMemo() {
        return (array_search($this->typeCode, array("G","M","P")) !== false);
    }

    /*public function __cmp__($other) {
        return strcmp($this->name, strtoupper((string)$other));
    }

    public function __hash__() {
        return hash($this->name);
    }*/

    public static function fromString($string, $start, $ignoreErrors=false) {
        // Decode dbf field definition from the string data.
        // 
        // Arguments:
        //     string:
        //         a string, dbf definition is decoded from. length of
        //         the string must be 32 bytes.
        //     start:
        //         position in the database file.
        //     ignoreErrors:
        //         initial error processing mode for the new field (boolean)

        if (strlen($string) != 32) {
            throw new InvalidArgumentException("The fromString function only accepts strings with 32 bytes");
        }
        $_length = ord($string[16]);
        $cls = get_called_class();
        $name = unzfill($string);
        $name = substr($name, 0, min(strlen($name), 10));
        return new $cls($name, $_length, ord($string[17]),
            $start, $start + $_length, $ignoreErrors);
    }

    public function toEncodeString() {
        // Return encoded field definition.
        // 
        // Return:
        //     Return value is a string object containing encoded
        //     definition of this field.

        $_name = str_pad($this->name, 11, chr(0));

        $start = $this->start;
        if (!isset($this->start))
            $start = 0;

        return (
            $_name .
            $this->typeCode .
            structPack("<L", array($start)) .
            chr($this->length) .
            chr($this->decimalCount) .
            str_pad("", 14, chr(0))
        );
    }

    public function toString() {
        $message = sprintf("%-10s %1s %3d %3d", $this->fieldInfo());
        return $message;
    }

    public function fieldInfo() {
        // Return field information.
        // 
        // Return:
        //     Return value is a (name, type, length, decimals) tuple.

        return array($this->name, $this->typeCode, $this->length, $this->decimalCount);
    }

    public function rawFromRecord($record) {
        // Return a "raw" field value from the record string.
        return substr($record, $this->start, $this->end-$this->start);
    }

    public function decodeFromRecord($record, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return decoded field value from the record string.
        try {
            return $this->decodeValue($this->rawFromRecord($record), $encodeIn, $encodeOut);
        } catch (Exception $e) {
            if ($this->ignoreErrors) {
                return getInvalidValue();
            } else {
                throw new Exception("decode error");
            }
        }
    }

    public function decodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return decoded value from string value.
        // 
        // This method shouldn't be used publicly. It's called from the
        // 'decodeFromRecord' method.

        // This is an abstract method and it must be overridden in child classes.
        
        throw new BadMethodCallException("The function 'decodeValue' is not implemented");
    }

    public function encodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return str object containing encoded field value.
        // 
        // This is an abstract method and it must be overriden in child classes.

        throw new BadMethodCallException("The function 'encodeValue' is not implemented");
    }
}

// real classes
class DbfCharacterFieldDef extends DbfFieldDef {
    // Definition of the character field.

    public $typeCode = "C";
    public $defaultValue = "";

    public function decodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return string object.
        // 
        // Return value is a "value" argument with stripped right spaces.
        if ($encodeIn !== $encodeOut) {
            $value = iconv($encodeIn, $encodeOut."//TRANSLIT", $value);
        }

        //FIXME: this is probably not the good way string termination is '\0'
        return rtrim($value, " ");
    }

    public function encodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return raw data string encoded from a "value".
        $cad = substr((string)$value, 0 , $this->length);

        if ($encodeIn !== $encodeOut) {
            $cad = iconv($encodeIn, $encodeOut."//TRANSLIT", $cad);
        }

        return str_pad($cad, $this->length);

    }
}

class DbfNumericFieldDef extends DbfFieldDef {
    // Definition of the numeric field.

    public  $typeCode = "N";
    //  XXX: now I'm not sure it was a good idea to make a class field
    //  'defaultValue' instead of a generic method as it was implemented
    //  previously -- it's ok with all types except number, cuz
    //  if $this->.decimalCount is 0, we should return 0 and 0.0 otherwise.
    public $defaultValue = 0;

    public function decodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return a number decoded from "value".
        // 
        // If decimals is zero, value will be decoded as an integer;
        // or as a float otherwise.
        // 
        // Return:
        //    Return value is a int (long) or float instance.

        try {
            $value = trim($value, " \0");
            if (strpos($value, ".") !== false) {
                # a float (has decimal separator)
                return floatval($value);
            } elseif ($value) {
                # must be an integer
                return intval($value);
            }
        } catch (Exception $e) {
            //Nothing to do...
        }
        return "";
    }

    public function encodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return string containing encoded "value".
        if (!isset($value))
            return str_pad("", $this->length);

        $format = sprintf("%d.%d", $this->length, $this->decimalCount);
        $_rv = sprintf("%".$format."f", $value);
        if (strlen($_rv) > $this->length) {
            $_ppos = strpos($_rv, ".");
            if ((0 <= $_ppos) && ($_ppos <= $this->length)) {
                $_rv = substr($_rv, 0, $this->length);
            } else {
                $error = sprintf("[%s] Numeric overflow: %s (field width: %i)", $this->name, $_rv, $this->length);
                throw new InvalidArgumentException($error);
            }
        }
        return $_rv;
    }
}

class DbfFloatFieldDef extends DbfNumericFieldDef {
    // Definition of the float field - same as numeric.

    public $typeCode = "F";
}

class DbfIntegerFieldDef extends DbfFieldDef {
    // Definition of the integer field.

    public $typeCode = "I";
    public $defaultValue = 0;
    public $length = 4;


    public function decodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return an integer number decoded from "value".
        $result = structUnpack("<i", $value);
        $result = (count($result) > 0) ? $result[0] : null;
        return $result;
    }

    public function encodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return string containing encoded "value".
        $result = structPack("<i", array(intval($value)));
        return $result;
   }
}

class DbfCurrencyFieldDef extends DbfFieldDef {
    // Definition of the currency field.

    public $typeCode = "Y";
    public $defaultValue = 0.0;
    public $length = 8;


    public function decodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return float number decoded from "value".
        $result = structUnpack("<q", $value);
        $result = (count($result) > 0) ? ($result[0] / 10000) : null;
        return $result;
    }

    public function encodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return string containing encoded "value".
        $result = structPack("<q", array(round($value * 10000)));
        return $result;
    }
}

class DbfLogicalFieldDef extends DbfFieldDef {
    // Definition of the logical field.

    public $typeCode = "L";
    public $defaultValue = -1;
    public $length = 1;

    public function decodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return true, false or -1 decoded from "value".
        // Note: value always is 1-char string
        if ($value === "?")
            return -1;
        if ($value === true)
            return true;
        if ($value === false)
            return false;
        if (array_search((string)strtoupper($value), array("N", "F", " ")) !== false)
            return false;
        if (array_search((string)strtoupper($value), array("Y", "T")) !== false)
            return true;
        $error = sprintf("[%s] Invalid logical value %r", $this->name, $value);
        throw new InvalidArgumentException($error);
    }

    public function encodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return a character from the "TF?" set.
        // 
        // Return:
        //     Return value is "T" if "value" is true
        //     "?" if value is -1 or false otherwise.
        if ($value === true)
            return "T";
        if (array_search((string)strtoupper($value), array("Y", "T")) !== false)
            return "T";
        if ($value == -1)
            return "?";
        return "F";
    }
}

class DbfMemoFieldDef extends DbfFieldDef {
    // Definition of the memo field.

    public $typeCode = "M";
    public $defaultValue = "\0\0\0\0";
    public $length = 4;

    //  MemoFile instance.  Must be set before reading or writing to the field.
    public $file = null;
    //  MemoData type for strings written to the memo file
    //protected $memoType = MemoData.TYPE_MEMO;

    public function decodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return MemoData instance containing field data.
        $_block = structUnpack("<L", $value)[0];
        if ($_block) {
            return $this->file.read($_block);
        }
        return new MemoData("", $this->memoType);
    }

    public function encodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return raw data string encoded from a "value".
        // 
        // Note: this is an internal method.

        if ($value) {
            $str = $this->file.write(new MemoData($value, $this->memoType));
            return structPack("<L", array($str));
        }
        return $this->defaultValue;
    }
}

class DbfGeneralFieldDef extends DbfFieldDef {
    // Definition of the general (OLE object) field.

    public $typeCode = "G";
    //protected $memoType = MemoData.TYPE_OBJECT;
}

class DbfDateFieldDef extends DbfFieldDef {
    // Definition of the date field.

    public $typeCode = "D";
    public $defaultValue = ""; //FIXME: Set Today as default value?
    //  "yyyymmdd" gives us 8 characters
    public $length = 8;

    public function decodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return a "datetime.date" instance decoded from "value".
        try {
            if (strlen(trim($value)) > 0) {
                return _getDate($value);
            }
        } catch (Exception $e) {
            //Nothing to do...
        }
        return "";
    }

    public function encodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return a string-encoded value.
        // 
        // "value" argument should be a value suitable for the
        // '_getDate' call.
        // 
        // Return:
        //     Return value is a string in format "yyyymmdd".

        if ($value) {
            return strftime("%Y%m%d", _getDate($value)->getTimestamp());
        }
        return str_pad("", $this->length, " ");
    }
}

class DbfDateTimeFieldDef extends DbfFieldDef {
    // Definition of the timestamp field.

    public $typeCode = "T";
    public $defaultValue = ""; //FIXME: Set Today as default value?
    //  two 32-bits integers representing JDN and amount of
    //  milliseconds respectively gives us 8 bytes.
    //  note, that values must be encoded in LE byteorder.
    public $length = 8;

    public function decodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return a 'datetime.datetime' instance.
        if (strlen($value) != $this->length) {
            throw new InvalidArgumentException("The value have not the correct size");
        }
        // LE byteorder
        $matriz = structUnpack("<2I", $value);
        $_jdn = $matriz[0];
        $_msecs = $matriz[1];
        if ($_jdn >= 1) {
            $dateArray = explode('/', jdtogregorian($_jdn));
            $dt  = str_pad($dateArray[2], 4, "0", STR_PAD_LEFT);
            $dt .= "-".str_pad($dateArray[0], 2, "0", STR_PAD_LEFT);
            $dt .= "-".str_pad($dateArray[1], 2, "0", STR_PAD_LEFT);
            $_rv = DBFDateTime::createFromFormat("Y-m-d H:i:s", $dt ." ". gmdate("H:i:s", $_msecs/1000));
        } else {
            // empty date
            $_rv = "";
        }
        return $_rv;
    }

    public function encodeValue($value, $encodeIn="UTF-8", $encodeOut="UTF-8") {
        // Return a string-encoded "value".
        if ($value) {
            $value = getDateTime($value);
            //  LE byteorder
            $year = $value->format('Y');
            $month = $value->format('m');
            $day = $value->format('d');
            $hour = $value->format('H');
            $minute = $value->format('i');
            $second = $value->format('s');

            $dateVal = gregoriantojd($month, $day, $year);
            $timeVal = (($hour * 3600) + ($minute * 60) + $second) * 1000;
            $_rv = structPack("<2I", array($dateVal, $timeVal));
        } else {
            $_rv = str_pad("", $this->length, chr(0));
        }
        if (strlen($_rv) !== $this->length) {
            throw new Exception("Invalid size");
        }
        return $_rv;
    }
}

$_fieldsRegistry = array();

function registerField($fieldCls) {
    global $_fieldsRegistry;
    // Register field definition class.
    // 
    // "fieldCls" should be subclass of the 'DbfFieldDef'.
    // 
    // Use 'lookupFor' to retrieve field definition class
    // by the type code.

    $classProperties = get_class_vars($fieldCls);
    if (!array_key_exists("typeCode", $classProperties)) {
        throw new Exception("Type code isn't defined");
    }

    //  XXX: use strtoupper(fieldCls->typeCode)? in case of any decign
    //  don't forget to look to the same comment in "lookupFor" method

    $_fieldsRegistry[$classProperties["typeCode"]] = $fieldCls;
}

function lookupFor($typeCode) {
    global $_fieldsRegistry;
    // Return field definition class for the given type code.
    // 
    // "typeCode" must be a single character. That type should be
    // previously registered.
    // 
    // Use 'registerField' to register new field class.
    // 
    // Return:
    //     Return value is a subclass of the 'DbfFieldDef'.

    //  XXX: use strtoupper(typeCode)? in case of any decign don't
    //  forget to look to the same comment in "registerField"

    return $_fieldsRegistry[$typeCode];
}

//  register generic types
$classes = get_declared_classes();
foreach ($classes as $class) {
    if (array_search('DbfFieldDef', class_parents($class)) !== false) {
        registerField($class);
    }
}
?>
