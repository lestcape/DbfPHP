<?php
// DBF record definition.

// History (most recent first):
// 11-feb-2007 [als]   __repr__: added special case for invalid field values
// 10-feb-2007 [als]   added .rawFromStream()
// 30-oct-2006 [als]   fix record length in .fromStream()
// 04-jul-2006 [als]   added export declaration
// 20-dec-2005 [yc]    DbfRecord.write() -> DbfRecord._write();
//                     added delete() method.
// 16-dec-2005 [yc]    record definition moved from 'dbf'.

// __version__ = "$Revision: 1.7 $"[11:-2]
// __date__ = "$Date: 2007/02/11 09:05:49 $"[7:-2]

// __all__ = ["DbfRecord"]

//import utils
require_once("./Utils.php");

class DbfRecord implements ArrayAccess, Countable {
    // DBF record.
    // 
    // Instances of this class shouldn't be created manualy,
    // use 'dbf.Dbf.newRecord' instead.
    // 
    // Class implements mapping/sequence interface, so
    // fields could be accessed via their names or indexes
    // (names is a preffered way to access fields).
    // 
    // Hint:
    //     Use 'store' method to save modified record.
    // 
    // Examples:
    //     Add new record to the database:
    //         $db = new Dbf($filename);
    //         $rec = $db.newRecord();
    //         $rec["FIELD1"] = $value1;
    //         $rec["FIELD2"] = $value2;
    //         $rec->store();
    //     Or the same, but modify existed
    //     (second in this case) record:
    //         $db = new Dbf($filename);
    //         $rec = $db[2];
    //         $rec["FIELD1"] = $value1;
    //         $rec["FIELD2"] = $value2;
    //         $rec->store();

    //  creation and initialization
    public $index;
    public $fieldData;//FIXME: Protected
    protected $dbf;
    protected $encode;
    protected $deleted;

    public function __construct($dbf, $encode=null, $index=null, $deleted=false, $data=null) {
        // Instance initialiation.
        // 
        // Arguments:
        //     $dbf:
        //         A 'Dbf.Dbf' instance this record belonogs to.
        //     $index:
        //         An integer record index or null. If this value is
        //         null, record will be appended to the DBF.
        //     $deleted:
        //         Boolean flag indicating whether this record
        //         is a deleted record.
        //     $data:
        //         A sequence or null. This is a data of the fields.
        //         If this argument is null, default values will be used.

        $this->dbf = $dbf;
        $this->encode = $encode;
        //  XXX: I'm not sure "index" is necessary
        $this->index = $index;
        $this->deleted = $deleted;
        if (!isset($data)) {
            $this->fieldData = array();
            foreach ($this->dbf->header->fields as $_fd) {
                array_push($this->fieldData, $_fd->defaultValue);
            }
        } else {
            $this->fieldData = $data;
        }
    }

    //  XXX: validate self.index before calculating position?
    public function getPosition() {
        $header = $this->dbf->header;
        return $header->headerLength + ($this->index * $header->recordLength);
    }

    // classmethods
    public static function rawFromStream($dbf, $index) {
        // Return raw record contents read from the stream.
        // 
        // Arguments:
        //     $dbf:
        //         A 'Dbf.Dbf' instance containing the record.
        //     $index:
        //         Index of the record in the records' container.
        //         This argument can't be null in this call.
        //     $encode:
        //         The record encode.
        // 
        // Return value is a string containing record data in DBF format.

        //  XXX: may be write smth assuming, that current stream
        //  position is the required one? it could save some
        //  time required to calculate where to seek in the file
        $header = $dbf->header;
        $recPos = $header->headerLength + ($index * $header->recordLength);
        fseek($dbf->stream, $recPos);

        $elem = fread($dbf->stream, $header->recordLength);
        return $elem;
    }

    public static function fromStream($dbf, $index, $encode="UTF-8") {
        // Return a record read from the stream.
        // 
        // Arguments:
        //     $dbf:
        //         A 'Dbf.Dbf' instance new record should belong to.
        //     $index:
        //         Index of the record in the records' container.
        //         This argument can't be null in this call.
        //     $encode:
        //         The record encode.
        // Return value is an instance of the current class.

        return DbfRecord::fromString($dbf, DbfRecord::rawFromStream($dbf, $index), $index, $encode);
    }

    public static function fromString($dbf, $string, $index=null, $encode="UTF-8") {
        // Return record read from the string object.
        // 
        // Arguments:
        //     $dbf:
        //         A 'Dbf.Dbf' instance new record should belong to.
        //     $string:
        //         A string new record should be created from.
        //     $index:
        //         Index of the record in the container. If this
        //         argument is null, record will be appended.
        //     $encode:
        //         The record encode.
        // 
        // Return value is an instance of the current class.
        $decodeStr = array();
        $encodeIn = $dbf->getEncode();
        foreach ($dbf->header->fields as $_fd) {
            array_push($decodeStr, $_fd->decodeFromRecord($string, $encodeIn, $encode));
        }
        return new DbfRecord($dbf, $encode, $index, ($string[0]==="*"), $decodeStr);
    }

    //  object representation
    public function toString() {
        $max = -1;
        $fieldNames = $this->dbf->getFieldNames();
        foreach ($fieldNames as $_fld) {
            if (strlen($_fld) > $max) {
                $max = strlen($_fld);
            }
        }
        $_template = sprintf("%%%ds: %%s (%%s)", $max);
        $_rv = array();
        foreach ($fieldNames as $_fld) {
            $_val = $this[$_fld];
            if ($_val === getInvalidValue()) {
                $val = sprintf($_template, $_fld, "null", "value cannot be decoded");
                array_push($_rv, $val);
            } else {
                if ($_val instanceof DateTime) {
                    $_val = $_val->format('Y-m-d');//H:i:s
                }
                $val = sprintf($_template, $_fld, $_val, gettype($_val));
                array_push($_rv, $val);
            }
        }
        return implode("\n", $_rv);
    }

    //  protected methods
    public function _write() {
        // Write data to the dbf stream.
        // 
        // FIXME:
        //     This isn't a public method, it's better to
        //     use 'store' instead publically.
        //     Be design "_write" method should be called
        //     only from the 'Dbf' instance.

        $this->_validateIndex(false);
        $recPos = $this->getPosition();
        fseek($this->dbf->stream, $recPos);
        $r = fwrite($this->dbf->stream, $this->toEncodeString());

        //  FIXME: may be move this write somewhere else?
        //  why we should check this condition for each record?
        if ($this->index == count($this->dbf)) {
            //  this is the last record,
            //  we should write SUB (ASCII 26)
            fwrite($this->dbf->stream, "\x1A");
        }
    }
    //  utility methods
    protected function _validateIndex($allowUndefined=true, $checkRange=false) {
        // Valid "$this->index" value.
        // 
        // If "allowUndefined" argument is true functions does nothing
        // in case of "$this->index" pointing to null object.

        if (!isset($this->index)) {
            if (!$allowUndefined)
                throw new UnexpectedValueException("Index is undefined");
        } elseif ($this->index < 0) {
            $error = sprintf("Index can't be negative (%s)", $this->index);
            throw new UnexpectedValueException($error);
        } elseif ($checkRange && ($this->index <= $this->dbf->header->recordCount)) {
            $error = sprintf("There are only %d records in the DBF", $this->dbf->header->recordCount);
            throw new UnexpectedValueException($error);
        }
    }

    // interface methods
    public function store() {
        // Store current record in the DBF.
        // 
        // If "$this->index" is null, this record will be appended to the
        // records of the DBF this records belongs to; or replaced otherwise.

        $this->_validateIndex();
        if (!isset($this->index)) {
            $this->index = count($this->dbf);
            $this->dbf->append($this);
        } else {
            $this->dbf[$this->index] = $this;
        }
    }

    public function delete() {
        // Mark method as deleted.
        $this->deleted = true;
    }

    public function toEncodeString() {
        // Return string packed record values.
        $encodeOut = $this->dbf->getEncode();
        $elements = array();
        foreach ($this->dbf->header->fields as $key => $_field) {
            $_dat = $this->fieldData[$key];
            array_push($elements, $_field->encodeValue($_dat, $this->encode, $encodeOut));
        }
        $chrDel = $this->deleted ? "*" : " ";
        $strRecord = $chrDel . implode("", $elements);
        return $strRecord;
    }

    public function asList() {
        // Return a flat list of fields.
        // 
        // Note:
        //     Change of the list's values won't change
        //     real values stored in this object.
        return $this->fieldData;//[:]
    }

    public function asDict() {
        // Return a dictionary of fields.
        // 
        // Note:
        //     Change of the dicts's values won't change
        //     real values stored in this object.
        $elements = array();
        foreach ($this->dbf->header->fields as $key => $_field) {
            $_dat = $this->fieldData[$key];
            $elements[$_field] = $_dat;
        }
        return $elements;
    }

    public function offsetSet($offset, $value) {
        if (is_null($offset)) {
            $this->fieldData[] = $value;
        } elseif (is_numeric($offset) && is_int($offset)) {
            $this->fieldData[$offset] = $value;
        } else {
            $index = $this->dbf->indexOfFieldName($offset);
            $this->fieldData[$index] = $value;
        }
    }

    public function offsetExists($offset) {
        if (is_numeric($offset) && is_int($offset)) {
             return isset($this->fieldData[$offset]);
        }
        $index = $this->dbf->indexOfFieldName($offset);
        return isset($this->fieldData[$index]);
    }

    public function offsetUnset($offset) {
        if (is_numeric($offset) && is_int($offset)) {
             unset($this->fieldData[$offset]);
        } else {
            $index = $this->dbf->indexOfFieldName($offset);
            unset($this->fieldData[$index]);
        }
    }

    public function offsetGet($offset) {
        // Return value by field name or field index.
        if (is_numeric($offset) && is_int($offset)) {
            //  integer index of the field
            return isset($this->fieldData[$offset]) ? $this->fieldData[$offset] : null;
        }
        //  assuming string field name
        $index = $this->dbf->indexOfFieldName($offset);
        return isset($this->fieldData[$index]) ? $this->fieldData[$index] : null;
    }

    public function count() {
        return count($this->fieldData);
    }
}
?>
