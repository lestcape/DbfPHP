<?php
// String utilities.
// TODO:
//   - allow strings in getDateTime routine;

// History (most recent first):
// 11-feb-2007 [als]   added INVALID_VALUE
// 10-feb-2007 [als]   allow date strings padded with spaces instead of zeroes
// 20-dec-2005 [yc]    handle long objects in getDate/getDateTime
// 16-dec-2005 [yc]    created from "strutil" module.

// __version__ = "$Revision: 1.4 $"[11:-2]
// __date__ = "$Date: 2007/02/11 08:57:17 $"[7:-2]

require_once("./Struct.php");

function structPack($fmt, $values) {
    $struct = new Struct();
    return $struct->pack($fmt, $values);
}

function structUnpack($fmt, $a, $p = 0, $byteArray = false) {
    $struct = new Struct();
    return $struct->unpack($fmt, $a, $p, $byteArray);
}

function unzfill($str) {
    // Return a string without ASCII NULs.
    // 
    // This function searchers for the first NUL (ASCII 0) occurance
    // and truncates string till that position.
    try {
        $pos = strpos($str, "\0");
        if($pos !== false) {
            $str = substr($str, 0, $pos);
            return $str;
        }
    } catch(Exception $e) {//ValueError
        //Do nothing...
    }
    return $str;
}

class DBFDate extends DateTime {
    function __construct($time = "now", $timezone = null) {
        parent::__construct($time, is_null($timezone) ? new DateTimeZone("UTC") : $timezone);
    }

    public static function createFromFormat($format, $time, $timezone = null) {
        return parent::createFromFormat($format, $time, $timezone);
    }

    public function __toString() {
        return $this->format("Y-m-d");
    }
}

class DBFDateTime extends DateTime {
    function __construct($time = "now", $timezone = null) {
        parent::__construct($time, is_null($timezone) ? new DateTimeZone("UTC") : $timezone);
    }

    public static function createFromFormat($format, $time, $timezone = null) {
        return parent::createFromFormat($format, $time, $timezone);
    }

    public function __toString() {
        return $this->format("Y-m-d H:i:s");
    }
}

function _getDate($date=null) {
    // Return 'datetime.date' instance.
    // 
    // Type of the "$date" argument could be one of the following:
    //     null:
    //         use current date value;
    //     datetime.date:
    //         this value will be returned;
    //     datetime.datetime:
    //         the result of the date.date() will be returned;
    //     string:
    //         assuming "%Y%m%d" or "%y%m%dd" format;
    //     number:
    //         assuming it's a timestamp (returned for example
    //         by the time.time() call;
    //     sequence:
    //         assuming (year, month, day, ...) sequence;
    // 
    // Additionaly, if "$date" has callable "ticks" attribute,
    // it will be used and result of the called would be treated
    // as a timestamp value.

    if (!isset($date)) {
        // use current value
        return new DBFDate("now");
    }
    if ($date instanceof DBFDate) {
        return $date;
    }
    if (is_numeric($date)) {
        // date is a timestamp
        return new DBFDate($date);
    }
    if (is_string($date)) {
        $date = str_replace(" ", "0", $date);
        if (strlen($date) == 6) {
            // yymmdd
            return DBFDate::createFromFormat("yymmdd", $date);
        }
        // yyyymmdd
        return DBFDate::createFromFormat("yyyymmdd", $date);
    }
    if (is_array($date)) {
        // a sequence (assuming date/time tuple)
        $dt  = str_pad($date[0], 4, "0", STR_PAD_LEFT);
        $dt .= "-".str_pad($date[1], 2, "0", STR_PAD_LEFT);
        $dt .= "-".str_pad($date[2], 2, "0", STR_PAD_LEFT);
        return DBFDate::createFromFormat("Y-m-d", $dt);
    }
    return new DBFDate("now");
    //return new DBFDate($date->ticks());
}

function getDateTime($date=null) {
    // Return 'datetime.datetime' instance.
    // 
    // Type of the "$date" argument could be one of the following:
    //     null:
    //         use current date value;
    //     datetime.date:
    //         result will be converted to the 'datetime.datetime' instance
    //         using midnight;
    //     datetime.datetime:
    //         "$date" will be returned as is;
    //     string:
    //         *** CURRENTLY NOT SUPPORTED ***;
    //     number:
    //         assuming it's a timestamp (returned for example
    //         by the time.time() call;
    //     sequence:
    //         assuming (year, month, day, ...) sequence;
    // 
    // Additionaly, if "$date" has callable "ticks" attribute,
    // it will be used and result of the called would be treated
    // as a timestamp value.

    if (!isset($date)) {
        //  use current value
        return new DateTime("now");
    }
    if ($date instanceof DateTime) {
        return $date;
    }
    if (is_numeric($date)) {
        # date is a timestamp
        return new DateTime($date);
    }
    if (is_string($date)) {
        $date = str_replace(" ", "0", $date);
        if (strlen($date) == 6) {
            # yymmdd
            return DateTime::createFromFormat("yymmdd", $date);
        }
        # yyyymmdd
        return DateTime::createFromFormat("yyyymmdd", $date);
    }
    if (is_array($date)) {
        # a sequence (assuming date/time tuple)
        $dt  = str_pad($date[0], 4, "0", STR_PAD_LEFT);
        $dt .= "-".str_pad($date[1], 2, "0", STR_PAD_LEFT);
        $dt .= "-".str_pad($date[2], 2, "0", STR_PAD_LEFT);
        $dt .= " ".str_pad($date[3], 2, "0", STR_PAD_LEFT);
        $dt .= ":".str_pad($date[4], 2, "0", STR_PAD_LEFT);
        $dt .= ":".str_pad($date[5], 2, "0", STR_PAD_LEFT);
        return DateTime::createFromFormat("Y-m-d H:i:s", $dt);
    }
    return new DateTime("now");
}

class _InvalidValue {
    // Value returned from DBF records when field validation fails
    // 
    // The value is not equal to anything except for itself
    // and equal to all empty values: null, 0, empty string etc.
    // In other words, invalid value is equal to null and not equal
    // to null at the same time.
    // 
    // This value yields zero upon explicit conversion to a number type,
    // empty string for string types, and false for boolean.

    public function __eq__($other) {
        return ($other === $this);
    }

    public function __ne__($other) {
        return  !($other === $this);
    }

    public function __nonzero__() {
        return false;
    }

    public function __int__() {
        return 0;
    }

    public function __long__() {
        return 0;
    }

    public function __float__() {
        return 0.0;
    }

    public function __str__() {
        return "";
    }

    public function __unicode__() {
        return "\u";
    }

    public function __repr__() {
        return "<INVALID>";
    }
}
//  invalid value is a constant singleton
$INVALID_VALUE = new _InvalidValue();

function getInvalidValue() {
   global $INVALID_VALUE;
   return $INVALID_VALUE;
}

// Supported ENCODES
$CODE_PAGES = array(
    0x01 => array(array('CP437'), "U.S. MS-DOS"),
    0x02 => array(array('CP850'), "International MS-DOS"),
    0x03 => array(array('WINDOWS-1252', 'CP1252'), "Window ANSI"),
    0x08 => array(array('CP865'), "Danish OEM"),
    0x09 => array(array('CP437'), "Dutch OEM"),
    0x0A => array(array('CP850'), "Dutch OEM*"),
    0x0B => array(array('CP437'), "Finnish OEM"),
    0x0D => array(array('CP437'), "French OEM"),
    0x0E => array(array('CP850'), "French OEM*"),
    0x0F => array(array('CP437'), "German OEM"),
    0x10 => array(array('CP850'), "German OEM*"),
    0x11 => array(array('CP437'), "Italian OEM"),
    0x12 => array(array('CP850'), "Italian OEM*"),
    0x13 => array(array('CP932'), "Japanese Shift-JIS"),
    0x14 => array(array('CP850'), "Spanish OEM*"),
    0x15 => array(array('CP437'), "Swedish OEM"),
    0x16 => array(array('CP850'), "Swedish OEM*"),
    0x17 => array(array('CP865'), "Norwegian OEM"),
    0x18 => array(array('CP437'), "Spanish OEM"),
    0x19 => array(array('CP437'), "English OEM (Britain)"),
    0x1A => array(array('CP850'), "English OEM (Britain)*"),
    0x1B => array(array('CP437'), "English OEM (U.S.)"),
    0x1C => array(array('CP863'), "French OEM (Canada)"),
    0x1D => array(array('CP850'), "French OEM*"),
    0x1F => array(array('CP852'), "Czech OEM"),
    0x22 => array(array('CP852'), "Hungarian OEM"),
    0x23 => array(array('CP852'), "Polish OEM"),
    0x24 => array(array('CP860'), "Portugese OEM"),
    0x25 => array(array('CP850'), "Potugese OEM*"),
    0x26 => array(array('CP866'), "Russian OEM"),
    0x37 => array(array('CP850'), "English OEM (U.S.)*"),
    0x40 => array(array('CP852'), "Romanian OEM"),
    0x4D => array(array('CP936'), "Chinese GBK (PRC)"),
    0x4E => array(array('CP949'), "Korean (ANSI/OEM)"),
    0x4F => array(array('CP950'), "Chinese Big 5 (Taiwan)"),
    0x50 => array(array('CP874'), "Thai (ANSI/OEM)"),
    0x57 => array(array('WINDOWS-1252', 'CP1252'), "ANSI"),
    0x58 => array(array('WINDOWS-1252', 'CP1252'), "Western European ANSI"),
    0x59 => array(array('WINDOWS-1252', 'CP1252'), "Spanish ANSI"),
    0x64 => array(array('CP852'), "Eastern European MS-DOS"),
    0x65 => array(array('CP866'), "Russian MS-DOS"),
    0x66 => array(array('CP865'), "Nordic MS-DOS"),
    0x67 => array(array('CP861'), "Icelandic MS-DOS"),
    0x6A => array(array('CP737'), "Greek MS-DOS (437G)"),
    0x6B => array(array('CP857'), "Turkish MS-DOS"),
    0x6C => array(array('CP863'), "French-Canadian MS-DOS"),
    0x78 => array(array('CP950'), "Taiwan Big 5"),
    0x79 => array(array('CP949'), "Hangul (Wansung)"),
    0x7A => array(array('CP936'), "PRC GBK"),
    0x7B => array(array('CP932'), "Japanese Shift-JIS"),
    0x7C => array(array('CP874'), "Thai Windows/MS-DOS"),
    0x86 => array(array('CP737'), "Greek OEM"),
    0x87 => array(array('CP852'), "Slovenian OEM"),
    0x88 => array(array('CP857'), "Turkish OEM"),
    0xC8 => array(array('CP1250'), "Eastern European Windows"),
    0xC9 => array(array('WINDOWS-1251', 'CP1251'), "Russian Windows"),
    0xCA => array(array('CP1254'), "Turkish Windows"),
    0xCB => array(array('CP1253'), "Greek Windows"),
    0xCC => array(array('CP1257'), "Baltic Windows"),
);

function getEncoding($codePage) {
    global $CODE_PAGES;
    if (!array_key_exists($codePage, $CODE_PAGES)) {
        return "UTF-8"; //Default encoding
    }
    return $CODE_PAGES[$codePage][0][0];
}

function getCodePage($encode) {
    global $CODE_PAGES;
    $encode = strtoupper($encode);
    foreach ($CODE_PAGES as $cp => $cData) {
        if (array_search($encode, $cData[0]) !== false) {
            return $cp;
        }
    }
    return 0x00;
}
?>
