<?php
// .DBF creation helpers.
// 
// Note: this is a legacy interface.  New code should use Dbf class
//       for table creation (see examples in dbf.py)
// 
// TODO:
//   - handle Memo fields.
//   - check length of the fields accoring to the
//     'http://www.clicketyclick.dk/databases/xbase/format/data_types.html'

// History (most recent first)
// 04-jul-2006 [als]   added export declaration;
//                     updated for dbfpy 2.0
// 15-dec-2005 [yc]    define DbfNew.__slots__
// 14-dec-2005 [yc]    added vim modeline; retab'd; added doc-strings;
//                     DbfNew now is a new class (inherited from object)
// ??-jun-2000 [--]    added by Hans Fiby

// __version__ = "$Revision: 1.4 $"[11:-2]
// __date__ = "$Date: 2006/07/04 08:18:18 $"[7:-2]

// __all__ = ["DbfNew"]

//from dbf import *
//from fields import *
//from header import *
//from record import *

require_once("./Dbf.php");
require_once("./Fields.php");
require_once("./Header.php");
require_once("./Record.php");
require_once("./Utils.php");

class _FieldDefinition {
    // Field definition.
    // 
    // This is a simple structure, which contains "name", "type",
    // "len", "dec" and "cls" fields.

    // Objects also implement get/setitem magic functions, so fields
    // could be accessed via sequence iterface, where 'name' has
    // index 0, 'type' index 1, 'len' index 2, 'dec' index 3 and
    // 'cls' could be located at index 4.

    public $name;
    protected $type;
    protected $len;
    protected $dec;
    protected $cls;

    // WARNING: be attentive - dictionaries are mutable!
    protected $FLD_TYPES = array(
        // type: (cls, len)
        "C" => array("DbfCharacterFieldDef", null),
        "N" => array("DbfNumericFieldDef", null),
        "L" => array("DbfLogicalFieldDef", 1),
        // FIXME: support memos
        // "M": array("DbfMemoFieldDef", null),
        "D" => array("DbfDateFieldDef", 8),
        // FIXME: I'm not sure length should be 14 characters!
        // but temporary I use it, cuz date is 8 characters
        // and time 6 (hhmmss)
        "T" => array("DbfDateTimeFieldDef", 14),
    );

    public function __construct($name, $type, $len=null, $dec=0) {
        $fData = $this->FLD_TYPES[$type];
        $_cls = $fData[0];
        $_len = $fData[1];
        if (!isset($_len)) {
            if (!isset($len)) {
                throw new UnexpectedValueException("Field length must be defined");
            }
            $_len = $len;
        }
        $this->name = $name;
        $this->type = $type;
        $this->len = $_len;
        $this->dec = $dec;
        $this->cls = $_cls;
    }

    public function getDbfField() {
        // Return 'DbfFieldDef' instance from the current definition.
        $class = $this->cls;
        return new $class($this->name, $this->len, $this->dec);
    }

    public function appendToHeader(&$dbfh) {
        // Create a 'DbfFieldDef' instance and append it to the dbf header.
        // 
        // Arguments:
        //     $dbfh: 'DbfHeader' instance.

        $_dbff = $this->getDbfField();
        $dbfh->addField($_dbff);
    }
}

class DbfNew {
    // New .DBF creation helper.
    // Example Usage:
    //     $dbfn = new DbfNew();
    //     $dbfn->addField("name",'C',80);
    //     $dbfn->addField("price",'N',10,2);
    //     $dbfn->addField("date",'D',8);
    //     $dbfn->write("tst.dbf");
    // Note:
    //     This module cannot handle Memo-fields,
    //     they are special.


    protected $fields;
    protected $encode;

    public function __construct($encode="UTF-8") {
        $this->fields = array();
        $this->encode = $encode;
    }

    public function addField($name, $typ, $len, $dec=0) {
        // Add field definition.
        // 
        // Arguments:
        //     $name:
        //         field name (string object). The field name must not
        //         contain ASCII nuls and it's length shouldn't
        //         exceed 10 characters.
        //     $typ:
        //         type of the field. This must be a single character
        //         from the "CNLMDT" set meaning character, numeric,
        //         logical, memo, date and date/time respectively.
        //     $len:
        //         length of the field. This argument is used only for
        //         the character and numeric fields. all other fields
        //         have fixed length.
        //         FIXME: use null as a default for this argument?
        //     $dec:
        //         decimal precision. Used only for the numric fields.

        array_push($this->fields, new _FieldDefinition($name, $typ, $len, $dec));
    }

    public function write($filename) {
        // Create empty .DBF file using current structure.
        $_dbfh = new DbfHeader(null, getCodePage($this->encode));
        $_dbfh->setCurrentDate();
        foreach ($this->fields as $_fldDef) {
            $_fldDef->appendToHeader($_dbfh);
        }
        $_dbfStream = fopen($filename, "wb");
        $_dbfh->write($_dbfStream);
        fclose($_dbfStream);
    }
}

if ((php_sapi_name() === "cli") && (get_included_files()[0] === __FILE__)) {
    // create a new DBF-File
    $dbfn = new DbfNew();
    $dbfn->addField("NAME", 'C', 80);
    $dbfn->addField("price", 'N', 10, 2);
    $dbfn->addField("date", 'D', 8);
    $dbfn->write("tst.dbf");
    echo "*** created tst.dbf: ***\n";
    // test new dbf
    $dbft = new Dbf("tst.dbf", false);
    echo $dbft->toString() . "\n";
    // add a record
    $rec = new DbfRecord($dbft);
    $rec["NAME"] = "something";
    $rec["price"] = 10.5;
    $rec["date"] = array(2000, 1, 12);
    $rec->store();
    // add another record
    $rec = new DbfRecord($dbft);
    $rec["NAME"] = "foo and bar";
    $rec["price"] = 12234;
    $rec["date"] = array(1992, 7, 15);
    $rec->store();
    echo "*** inserted 2 records into tst.dbf: ***\n";
    // show the records
    echo $dbft->toString(). "\n";
    $fieldNames = $dbft->getFieldNames();

    foreach ($dbft as $p => $rec) {
        foreach ($fieldNames as $fldName) {
            echo sprintf("%s:\t %s\n", $fldName, $rec[$fldName]);
        }
    }

    $dbft->close();
}
?>
