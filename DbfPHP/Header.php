<?php
// DBF header definition.
// 
// TODO:
//   - handle encoding of the character fields
//     (encoding information stored in the DBF header)

// History (most recent first):
// 14-dec-2010 [als]   added Memo file support
// 16-sep-2010 [als]   fromStream: fix century of the last update field
// 11-feb-2007 [als]   added .ignoreErrors
// 10-feb-2007 [als]   added __getitem__: return field definitions
//                     by field name or field number (zero-based)
// 04-jul-2006 [als]   added export declaration
// 15-dec-2005 [yc]    created

// __version__ = "$Revision: 1.7 $"[11:-2]
// __date__ = "$Date: 2010/12/14 11:07:45 $"[7:-2]

// __all__ = ["DbfHeader"]

//import cStringIO
//import datetime
//import struct
//import time

//import fields
//from utils import getDate

require_once("./Fields.php");
require_once("./Utils.php");


class DbfHeader implements ArrayAccess, Countable {
    // Dbf header definition.
    // 
    // For more information about dbf header format visit
    // 'http://www.clicketyclick.dk/databases/xbase/format/dbf.html#DBF_STRUCT'
    // 
    // Examples:
    //     Create an empty dbf header and add some field definitions:
    //         $dbfh = new DbfHeader();
    //         $dbfh->addField(array("name", "C", 10));
    //         $dbfh->addField(array("date", "D"));
    //         $dbfh->addField(new DbfNumericFieldDef("price", 5, 2));
    //     Create a dbf header with field definitions:
    //         dbfh = new DbfHeader(array(
    //             array("name", "C", 10),
    //             array("date", "D"),
    //             new DbfNumericFieldDef("price", 5, 2),
    //         ));

    //  instance construction and initialization methods
    public $fields;
    public $recordCount;
    public $headerLength;
    public $recordLength;

    protected $signature;
    protected $codePage;
    protected $lastUpdate;


    protected $changed;
    protected $ignoreErrors;

    public function __construct($fields=null, $codePage=0x00, $headerLength=0, $recordLength=0,
        $recordCount=0, $signature=0x03, $lastUpdate=null, $ignoreErrors=false) {
        // Initialize instance.
        // 
        // Arguments:
        //     $fields:
        //         a list of field definitions;
        //     $recordLength:
        //         size of the records;
        //     $headerLength:
        //         size of the header;
        //     $recordCount:
        //         number of records stored in DBF;
        //     $signature:
        //         version number (aka signature). using 0x03 as a default meaning
        //         "File without DBT". for more information about this field visit
        //         'http://www.clicketyclick.dk/databases/xbase/format/dbf.html#DBF_NOTE_1_TARGET'
        //     $codePage:
        //         The codepage, this is the file encode.
        //     $lastUpdate:
        //         date of the DBF's update. this could be a string ('yymmdd' or
        //         'yyyymmdd'), timestamp (int or float), datetime/date value,
        //         a sequence (assuming (yyyy, mm, dd, ...)) or an object having
        //         callable "ticks" field.
        //     ignoreErrors:
        //         error processing mode for DBF fields (boolean)

        $this->signature = $signature;
        $this->codePage = $codePage;
        if (!isset($fields)) {
            $this->fields = array();
        } else {
            $this->fields = $fields;
        }
        $this->lastUpdate = _getDate($lastUpdate);
        $this->recordLength = $recordLength;
        $this->headerLength = $headerLength;
        $this->recordCount = $recordCount;
        $this->ignoreErrors = $ignoreErrors;
        //  XXX: I'm not sure this is safe to
        //  initialize '$this->changed' in this way
        $this->changed = (count($this->fields) > 0);
    }

    public function isChanged() {
        return $this->changed;
    }

    // classmethods
    public static function fromString($string, $codePage=0x00) {
        // Return header instance from the string object.
        $stream = fopen('php://memory','r+');
        fwrite($stream, (string)$string);
        return DbfHeader::fromStream($stream, $codePage);
    }

    public static function fromStream($stream, $codePage=0x00) {
        $_resultCodePage = $codePage;
        // Return header object from the stream.
        // In PHP: fseek($stream, $offset, $whence) === -1
        // Or probably better: rewind($stream);
        fseek($stream, 0, 0);
        $_data = fread($stream, 32);

        if (($_data == null) || (strlen($_data) < 32)) {
            $error = "The header data less than 32 bytes";
            throw new OutOfBoundsException($error);
        }
        $matriz = structUnpack("<4BI2H16x2B2x", $_data);
        $_signature = $matriz[0];
        $_year = $matriz[1];
        $_month = $matriz[2];
        $_day = $matriz[3];
        $_cnt = $matriz[4];
        $_hdrLen = $matriz[5];
        $_recLen = $matriz[6];
        $_reserved = $matriz[7];
        $_flag = $matriz[8];
        $_codePage = $matriz[9];

        if(($_codePage !== 0x00) && ($_codePage != $codePage)) {
            $_resultCodePage = $_codePage;
        }
        if ($_year < 80) {
            //  dBase II started at 1980.  It is quite unlikely
            //  that actual last update date is before that year.
            $_year += 2000;
        } else {
            $_year += 1900;
        }
        //  create header object
        $date = array($_year, $_month, $_day);
        $_obj = new DbfHeader(null, $_resultCodePage, $_hdrLen, $_recLen, $_cnt, $_signature, $date);

        //  append field definitions
        //  position 0 is for the deletion flag
        $_pos = 1;
        $_data = fread($stream, 1);
        while ($_data[0] != "\x0D") {
            $_data .= fread($stream, 31);
            $class = lookupFor($_data[11]);
            $_fld = $class::fromString($_data, $_pos);
            $_obj->_addField($_fld);
            $_pos = $_fld->end;
            $_data = fread($stream, 1);
        }
        return $_obj;
    }

    //  properties
    public function getYear() {
        return intval($this->lastUpdate->format('Y'));
    }

    public function getMonth() {
        return intval($this->lastUpdate->format('m'));
    }

    public function getDay() {
        return intval($this->lastUpdate->format('d'));
    }

    public function hasMemoField() {
        // true if at least one field is a Memo field
        foreach ($this->fields as $_field) {
            if ($_field->isMemo()) {
                return true;
            }
        }
        return false;
    }

    // Error processing mode for DBF field value conversion
    // if set, failing field value conversion will return
    // "INVALID_VALUE" instead of raising conversion error.
    public function setIgnoreErrors($value) {
        // Update 'ignoreErrors' flag on $this and all fields
        $value = bool($value);
        $this->ignoreErrors = $value;
        foreach ($this->fields as $_field) {
            $_field->ignoreErrors = $value;
        }
    }

    public function getIgnoreErrors() {
        return $this->ignoreErrors;
    }

    //  object representation
    public function toString() {
        $_rv  = "Version (signature): 0x%02x\n";
        $_rv .= "          Code page: 0x%02x\n";
        $_rv .= "             Encode: %s\n";
        $_rv .= "        Last update: %s\n";
        $_rv .= "      Header length: %d\n";
        $_rv .= "      Record length: %d\n";
        $_rv .= "       Record count: %d\n";
        $_rv .= " FieldName Type Len Dec\n";

        $_rv = sprintf($_rv, $this->signature, $this->codePage, getEncoding($this->codePage), $this->lastUpdate->format('Y-m-d'),// H:i:s
                       $this->headerLength, $this->recordLength, $this->recordCount);
        foreach ($this->fields as $_fld) {
            $info = $_fld->fieldInfo();
            $name = $info[0]; $typeCode = $info[1];
            $length = $info[2]; $decimalCount = $info[3];
            $message = sprintf("%10s %4s %3s %3s", $name, $typeCode, $length, $decimalCount);
            $_rv .= $message."\n";
        }
        return $_rv;
    }

    // internal methods
    protected function _addField(&$defs) {
        // Internal variant of the 'addField' method.
        // 
        // This method doesn't set '$this->changed' field to true.
        // 
        // Return value is a length of the appended records.
        // Note: this method doesn't modify "recordLength" and
        // "headerLength" fields. Use 'addField' instead of this
        // method if you don't exactly know what you're doing.

        //  insure we have dbf.DbfFieldDef instances first (instantiation
        //  from the tuple could throw an error, in such a case I don't
        //  wanna add any of the definitions -- all will be ignored)
        $_defs = [];
        $_recordLength = $this->recordLength;
        if (is_array($defs)) {
            foreach ($defs as $_def) {
                if ($_def instanceof DbfFieldDef) {
                    $_obj = $_def;
                } else {
                    for ($x = count($_def); $x <= 4; $x++) {
                        array_push($_def, null);
                    }
                    $_name = $_def[0];
                    $_type = $_def[1];
                    $_len = $_def[2];
                    $_dec = $_def[3];
                    $_cls = lookupFor($_type);
                    $_obj = new $_cls($_name, $_len, $_dec, $_recordLength, $this->ignoreErrors);
                }
                $_recordLength += $_obj->length;
                array_push($_defs, $_obj);
            }
        } elseif ($defs instanceof DbfFieldDef) {
            $_obj = $defs;
            $_recordLength += $_obj->length;
            array_push($_defs, $_obj);
        }
        //  and now extend field definitions and
        //  update record length
        $this->fields = array_merge($this->fields, $_defs);

        return ($_recordLength - $this->recordLength);
    }

    protected function _calcHeaderLength() {
        // Update $this->headerLength attribute after change to header contents
        // recalculate headerLength
        $_hl = 32 + (32 * count($this->fields)) + 1;
        if ($this->signature == 0x30) {
            //  Visual FoxPro files have 263-byte zero-filled field for backlink
            $_hl += 263;
        }
        //  bug#15: don't reduce existing header size
        if ($_hl > $this->headerLength) {
            $this->headerLength = $_hl;
        }
    }

    //  interface methods
    public function setMemoFile($memo) {
        // Attach MemoFile instance to all memo fields; check header signature

        $_has_memo = false;
        foreach ($this->fields as $_field) {
            if ($_field->isMemo()) {
                $_field->file = $memo;
                $_has_memo = true;
            }
        }
        //  for signatures list, see
        //  http://www.clicketyclick.dk/databases/xbase/format/dbf.html#DBF_NOTE_1_TARGET
        //  http://www.dbf2002.com/dbf-file-format.html
        //  If memo is attached, will use 0x30 for Visual FoxPro file,
        //  0x83 for dBASE III+.

        $sig = array(0x30, 0x83, 0x8B, 0xCB, 0xE5, 0xF5);
        if ($_has_memo && (array_search($this->signature, $sig) === false)) {
            if ($memo.is_fpt) {
                $this->signature = 0x30;
            } else {
                $this->signature = 0x83;
            }
        }
        $this->_calcHeaderLength();
    }

    public function addField(&$defs) {
        // Add field definition to the header.
        // 
        // Examples:
        //     $dbfh->addField(array(
        //         array("name", "C", 20),
        //         new DbfCharacterFieldDef("surname", 20),
        //         new DbfDateFieldDef("birthdate"),
        //         array("member", "L"),
        //     ));
        //     $dbfh->addField(array("price", "N", 5, 2))
        //     $dbfh->addField(new DbfNumericFieldDef("origprice", 5, 2))

        if (!isset($this->recordLength) || $this->recordLength < 1) {
            $this->recordLength = 1;
        }

        $this->recordLength += $this->_addField($defs);
        $this->_calcHeaderLength();
        $this->changed = true;
    }

    public function write($stream) {
        // Encode and write header to the stream.
        fseek($stream, 0, 0);
        fwrite($stream, $this->toEncodeString());
        $fieldsList = "";
        foreach ($this->fields as $_fld) {
            $fieldsList .= $_fld->toEncodeString();
        }
        fwrite($stream, $fieldsList);
        fwrite($stream, chr(0x0D)); // cr at end of all hdr data
        $_pos = ftell($stream);
        if ($_pos < $this->headerLength) {
            fwrite($stream, str_pad("", $this->headerLength - $_pos, chr(0)));
        }
        $this->changed = false;
    }

    public function toEncodeString() {
        //  Returned 32 chars length string with encoded header.
        //  FIXME: should keep flag and code page marks read from file
        if ($this->hasMemoField()) {
            $_flag = "\x02";
        } else {
            $_flag = "\0";
        }
        //$this->codePage = "\x03";
        $_packedData = structPack("<4BI2H", array(
            $this->signature,
            $this->getYear() - 1900,
            $this->getMonth(),
            $this->getDay(),
            $this->recordCount,
            $this->headerLength,
            $this->recordLength,
        ));
        return $_packedData . str_pad("", 16, chr(0)) . $_flag . chr($this->codePage) . "\0\0";
    }

    public function setCurrentDate() {
        // Update "$this->lastUpdate" field with current date value.
        $this->lastUpdate = new DateTime("now");
    }

    // Magic Method
    public function count() {
        return $this->recordCount;
    }

    public function offsetSet($offset, $value) {
        /*if (is_null($offset)) {
            $this->fieldData[] = $value;
        } else {
            $this->fieldData[$offset] = $value;
        }*/
    }

    public function offsetExists($offset) {
        if (is_numeric($offset) && is_int($offset)) {
             return isset($this->fields[$offset]);
        }
        //  assuming string field name
        $_name = $offset.upper();
        foreach ($this->fields as $pos => $_field) {
            if ($_field.name === $_name) {
                return true;
            }
        }
        return false;
    }

    public function offsetUnset($offset) {
        if (is_numeric($offset) && is_int($offset)) {
            unset($this->fields[$offset]);
        } else {
            //  assuming string field name
            $_name = $offset.upper();
            foreach ($this->fields as $pos => $_field) {
                if ($_field.name === $_name) {
                    unset($this->fields[$pos]);
                    break;
                }
            }
        }
    }

    public function offsetGet($offset) {
        // Return value by field name or field index.
        if (is_numeric($offset) && is_int($offset)) {
            //  integer index of the field
            return isset($this->fields[$offset]) ? $this->fields[$offset] : null;
        }
        //  assuming string field name
        $_name = $offset.upper();
        foreach ($this->fields as $_field) {
            if ($_field.name === $_name) {
                return $_field;
            }
        }
        $error = sprintf("The item %s is not a key value", $item);
        throw new OutOfBoundsException($error);
    }
}
?>
