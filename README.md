# DbfPHP
This is a code to read and write XBase (dbase) files on PHP.

**!!  USE AT YOUR OWN RISK    !!**

**!! NO WARRANTIES WHATSOEVER !!**

Examples:
--------------
```php
    //     Create new table, setup structure, add records:
    // 
    $_dbf = new Dbf($filename, false, true, "Windows-1252");
    $fields = array(
        array("NAME", "C", 15),
        array("SURNAME", "C", 25),
        array("INITIALS", "C", 10),
        array("BIRTHDATE", "D"),
        array("ISMAN", "L"),
        array("STATURE", "N", 10, 2),
        array("WEIGHT", "F", 5, 3),
        array("AGE", "I"),
        array("SIZE", "Y"),
        array("DATE", "T"),
    );
    $_dbf->addField($fields);
    $records = array(
        array(
            "John", "Miller", "YC", array(1981, 1, 2), true,
            10.5, 10.5, -1, 10.5, array(1981, 1, 2, 3, 33, 0)
        ),
        array(
            "Andy", "Larkin", "AL", array(1982, 3, 4), false,
            12234, 12234, 0, 12234, array(1981, 1, 2, 10, 33, 0)
        ),
        array(
            "Bill", "Clinth", "", array(1983, 5, 6), "f", 0, 0,
            100, 0, array(1981, 1, 2, 10, 33, 0)
        ),
        array(
             "Bobb", "McNail", "", array(1984, 7, 8), "t", -1,
             -3, 99, -333, array(1981, 1, 2, 10, 33, 0)
        ),
    );
    foreach ($records as $record) {
        $_rec = $_dbf->newRecord();
        $_rec["NAME"] = $record[0];
        $_rec["SURNAME"] = $record[1];
        $_rec["INITIALS"] = $record[2];
        $_rec["BIRTHDATE"] = $record[3];
        $_rec["ISMAN"] = $record[4];
        $_rec["STATURE"] = $record[5];
        $_rec["WEIGHT"] = $record[6];
        $_rec["AGE"] = $record[7];
        $_rec["SIZE"] = $record[8];
        $_rec["DATE"] = $record[9];
        $_rec->store();
    }
    echo $_dbf->toString();
    $_dbf->close();
```
```php
    //    Open existed dbf, read some data:
    // 
    $_dbf = new Dbf($filename, true, false, "Windows-1252");
    foreach ($_dbf as $_rec) {
        echo $_rec->toString() . "\n\n";
    }
    $_dbf->close();
```

Known Issues
--------------
- Not memo support yet.
- Not complex binary data (W type field).

Credits:
--------------

Is base on the work of the python modules [dbfpy](http://dbfpy.sourceforge.net) by:

    1- Jeff Kunce <kuncej@mail.conservation.state.mo.us> http://starship.python.net/crew/jjkunce/

    2- Hans Fiby <hans@fiby.at> http://www.fiby.at

It use the code of [structphp](https://github.com/desean1625/structphp) by:

    1-Sean Sullivan [@desean1625](https://github.com/desean1625)

Previous Changelog
--------------

Version 2.3.1 (09-aug-2015)
 - **Fixes:** db.close() for files without Memo fields (bug#16).
 - **Fixes:** Don't reduce existing header size when a Memo file is attached (bug#15).

Version 2.3.0 (29-jun-2014)
 - **Features:** Support Memo files.
 - **Features:** Support getting a slice of a dbf object.

Version 2.2.5 (16-sep-2010)
 - **Fixes:** Y2K issue with Last Update field in the header (sf bug 3065838).

Version 2.2.4 (26-may-2009)
 - **Fixes:** Ignore leading and trailing zero bytes in numeric fields.

Version 2.2.3 (17-feb-2009)
 - **Features:** Support for writing empty date values.

Version 2.2.2 (16-sep-2008)
 - **Fixes:** Numeric decoder returns float when the value contains decimal point without regard to the number of decimal places declared in the header.

Version 2.2.1 (16-mar-2008)
 - **Fixes:** raise ValueError if a name passed to field constructor is longer than 10 characters.

Version 2.2.0 (11-feb-2007)
 - **Features:** date fields are allowed to contain values stored with leading spaces instead of leading zeroes.
 - **Features:**  dbf.header returns field definition objects if accessed as a list or as a dictionary.
 - **Features:** added raw data access methods: DbfRecord.rawFromStream(), DbfFieldDef.rawFromRecord().
 - **Features:** added conversion error handling: if ignoreErrors=True is passed with Dbf constructor arguments, then failing field value conversions will not raise errors but instead will return special object INVALID_VALUE which is equal to None, empty string and zero.
 - **Features:**  .ignoreErrors property of the Dbf instances may be toggled also after instance initialization.

Version 2.1.0 (01-dec-2006)
 - **Features:** support field types 'F' (float), 'I' (integer) and 'Y' (currency)
 - **Fixes:** processing of empty Timestamp values

Version 2.0.3 (30-oct-2006)
  - **Fixes:** compatibility fix for Python versions prior to 2.4 (sf bug 1574526)
  - **Fixes:** wrong record length when reading from file object (sf bug 1586619)

Version 2.0.2 (08-jul-2006)
  - **Fixes:** dbfnew (legacy API for DBF creation)

Version 2.0.1 (10-mar-2006)
  - **Fixes:** in Numeric field processing, decoding inserted decimal point where it shouldn't;
  - **Fixes:** in Numeric field processing, encoded value never exceeds field length.
  - **Fixes:** in Numeric field processing, If encoded value is too large for the field, decimal digits get removed (rounding the value down).  When integer value is larger than the field length, ValueError is raised.
  - **Fixes:** Date and Logical fields were not able to decode empty values.
  - **Features:** Errors raised from field processing include field name.

Version 2.0.0 (20-dec-2005)
  - Changed style of doc-strings; added more docs.
  - **Features:** ```mx.DateTime``` isn't required anymore - internal representation uses ```datetime.date``` and ```datetime.datetime``` classes from the batteries.
  - **Features:** ```D``` and ```T``` became "smarter"
  - Changed identifiers and args of some methods. For example old code
  - All classes now are new-style classes (inherited from the ```object```).
  - Got rid of ```binnum``` and ```strutil``` modules, using ```struct``` and string methods instead.
  - Group classes in different modules instead of having them all in one, as it was earlier (see dbf.py from the previous release(s)).
  - Use fields' registry to simplify registering of the new data types.

Version old (2000-10-06)
 - took over development 2000-10-06 Hans Fiby

Version old (2001-02-07 Hans Fiby)
 - **Features:** read long integers as long
 - **Features:** add this CHANGES File

Version old (2000-10-06 Hans Fiby)
 - **Features:** added dbfnew.py
 - **Fixes:** decimal points
